﻿namespace Algorithms.Interfaces
{
    /// <summary>
    /// Provides methods for calculating time.
    /// </summary>
    public interface ITimeCalculator
    {
        /// <summary>
        /// Start calculations.
        /// </summary>
        void Start();
        
        /// <summary>
        /// Stops calculation.
        /// </summary>
        void Stop();
        
        /// <summary>
        /// Returns elapsed time.
        /// </summary>
        /// <returns>Elapsed time.</returns>
        long GetElapsedTime();
    }
}