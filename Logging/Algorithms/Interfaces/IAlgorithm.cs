﻿namespace Algorithms.Interfaces
{
    /// <summary>
    /// Provides method for calculating GCD using specified algorithm.
    /// </summary>
    public interface IAlgorithm
    {
        /// <summary>
        /// Calculates GCD of two numbers.
        /// </summary>
        /// <param name="first">First number.</param>
        /// <param name="second">Second number.</param>
        /// <returns>GCD of two numbers.</returns>
        int Calculate(int first, int second);
    }
}