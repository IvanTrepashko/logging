﻿using System.Diagnostics;
using Algorithms.Interfaces;

namespace Algorithms.Implementations
{
    /// <summary>
    /// Represents class for calculating time using <see cref="Stopwatch"/>.
    /// </summary>
    public class StopwatchTimeCalculator : ITimeCalculator
    {
        private Stopwatch timer;
        
        /// <inheritdoc cref="ITimeCalculator"/>
        public void Start()
        {
            this.timer = Stopwatch.StartNew();
        }

        /// <inheritdoc cref="ITimeCalculator"/>
        public void Stop()
        {
            this.timer.Stop();
        }
        
        /// <inheritdoc cref="ITimeCalculator"/>
        public long GetElapsedTime()
        {
            return this.timer.ElapsedMilliseconds;
        }
    }
}