﻿using Algorithms.Interfaces;
using Microsoft.Extensions.Logging;

namespace Algorithms.Implementations
{
    public class AlgorithmTimeDecorator : IAlgorithm
    {
        private IAlgorithm algorithm;
        private ITimeCalculator timer;
        private long elapsedTime;
        
        
        public AlgorithmTimeDecorator(IAlgorithm algorithm, ITimeCalculator timer)
        {
            this.algorithm = algorithm;
            this.timer = timer;
        }
        
        public int Calculate(int first, int second)
        {
            this.timer.Start();
            
            int result = this.algorithm.Calculate(first, second);
            
            this.timer.Stop();
            
            this.elapsedTime = this.timer.GetElapsedTime();
            return result;
        }

        public long GetElapsedTime()
        {
            return this.elapsedTime;
        }
    }
}