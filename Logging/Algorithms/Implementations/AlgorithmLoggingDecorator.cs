﻿using Algorithms.Interfaces;
using Microsoft.Extensions.Logging;

namespace Algorithms.Implementations
{
    public class AlgorithmLoggingDecorator : IAlgorithm
    {
        private ILogger logger;
        private IAlgorithm algorithm;
        
        public AlgorithmLoggingDecorator(IAlgorithm algorithm, ILogger logger)
        {
            this.algorithm = algorithm;
            this.logger = logger;
        }

        public int Calculate(int first, int second)
        {
            this.logger.LogInformation("Calculate method started.");
            
            int result = this.algorithm.Calculate(first, second);
            
            this.logger.LogInformation("Calculate method ended");
            this.logger.LogInformation($"Result of calculation is {result}.");
            
            return result;
        }
    }
}