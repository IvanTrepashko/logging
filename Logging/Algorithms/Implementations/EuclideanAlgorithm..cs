﻿using System;
using Algorithms.Interfaces;
using Microsoft.Extensions.Logging;

namespace Algorithms.Implementations
{
    /// <summary>
    /// Represents class for calculating GCD using Euclidean algorithm.
    /// </summary>
    public class EuclideanAlgorithm : IAlgorithm
    {
        /// <summary>
        /// Calculates GCD of two given numbers.
        /// </summary>
        /// <param name="first">First number.</param>
        /// <param name="second">Second number.</param>
        /// <returns>GCD of two numbers.</returns>
        public int Calculate(int first, int second)
        {
            
            if (first == 0 && second == 0)
            {
                var ex = new ArgumentException("Both parameters are equal to 0.");
                throw ex;
            }

            if (first == int.MinValue || second == int.MinValue)
            {
                var ex = new ArgumentOutOfRangeException($"{nameof(first)} or {nameof(second)} is equal to int.MinValue");
                throw ex;
            }

            if (first == 0 || second == 0)
            {
                return Math.Abs(first + second);
            }

            while (true)
            {
                if ((first %= second) == 0)
                {
                    return Math.Abs(second);
                }

                if ((second %= first) == 0)
                {
                    return Math.Abs(first);
                }
            }
        }
    }
}