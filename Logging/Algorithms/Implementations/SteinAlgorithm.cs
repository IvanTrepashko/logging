﻿using System;
using Algorithms.Interfaces;
using Microsoft.Extensions.Logging;

namespace Algorithms.Implementations
{
    /// <summary>
    /// Represents class for calculating GCD using Stein algorithm.
    /// </summary>
    public class SteinAlgorithm : IAlgorithm
    {
        /// <inheritdoc/>
        public int Calculate(int first, int second)
        {
            
            if (first == 0 && second == 0)
            {
                var ex = new ArgumentException("Both parameters are equal to 0.");
                throw ex;
            }

            if (first == int.MinValue || second == int.MinValue)
            {
                var ex = new ArgumentOutOfRangeException($"{nameof(first)} or {nameof(second)} is equal to int.MinValue");
                throw ex;
            }
            
            first = Math.Abs(first);
            second = Math.Abs(second);

            if (first == 0)
            {
                return second;
            }

            if (second == 0)
            {
                return first;
            }

            if (second % 2 == 0 && first % 2 == 0)
            {
                return 2 * this.Calculate(first / 2, second / 2);
            }

            if (second % 2 == 0)
            {
                return this.Calculate(first, second / 2);
            }

            return first % 2 == 0 ? this.Calculate(first / 2, second) : this.Calculate(Math.Abs(first - second), Math.Min(first, second));
        }
    }
}