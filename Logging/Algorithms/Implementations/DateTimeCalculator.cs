﻿using System;
using Algorithms.Interfaces;

namespace Algorithms.Implementations
{
    /// <summary>
    /// Represents class for calculating elapsed time using <see cref="DateTime"/>.
    /// </summary>
    public class DateTimeCalculator : ITimeCalculator
    {
        private DateTime _start;
        private DateTime _end;
        
        /// <inheritdoc cref="ITimeCalculator"/>
        public void Start()
        {
            this._start = DateTime.Now;
        }

        /// <inheritdoc cref="ITimeCalculator"/>
        public void Stop()
        {
            this._end = DateTime.Now;
        }
        
        /// <inheritdoc cref="ITimeCalculator"/>
        public long GetElapsedTime()
        {
            return this._end.Ticks - this._start.Ticks;
        }
    }
}