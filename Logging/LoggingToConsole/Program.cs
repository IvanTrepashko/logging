﻿using System;
using Algorithms.Implementations;
using Algorithms.Interfaces;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;

namespace LoggingToConsole
{
    public static class Program
    {
        static void Main(string[] args)
        {
            var factory = LoggerFactory.Create(
                builder =>
                {
                    builder.AddConsole();
                });

            var factory1 = LoggerFactory.Create(
                builder =>
                {
                    builder.AddNLog();
                });
            
            var euclideanLogger = factory.CreateLogger("euclideanLogger");
            IAlgorithm euclideanAlgorithm = new EuclideanAlgorithm();
            IAlgorithm euclideanLoggerDecorator = new AlgorithmLoggingDecorator(euclideanAlgorithm, euclideanLogger);

            var steinLogger = factory1.CreateLogger<SteinAlgorithm>();
            IAlgorithm steinAlgorithm = new SteinAlgorithm();
            IAlgorithm steinLoggerDecorator = new AlgorithmLoggingDecorator(steinAlgorithm, steinLogger);

            euclideanLoggerDecorator.Calculate(50, 10);
            steinLoggerDecorator.Calculate(100, 15);


            AlgorithmTimeDecorator timeDecorator =
                new AlgorithmTimeDecorator(euclideanAlgorithm, new DateTimeCalculator());

            timeDecorator.Calculate(15_000, 25_000);

            long elapsedTime = timeDecorator.GetElapsedTime();
        }
    }
}